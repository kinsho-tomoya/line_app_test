// 'use strict';
require('dotenv').config(); //環境変数の読み込み
const express = require('express');
const line = require('@line/bot-sdk');
const PORT = process.env.PORT || 3000;
const connection = require('./mysqlConnection');
const wrapper = require('node-mysql-wrapper');
const db = wrapper.wrap(connection);
const app = express();
const lineConfig = {
    channelSecret: process.env.LINE_CHANNEL_SECRET_TOKE,
    channelAccessToken: process.env.LINE_CHANNEL__TOKE
};


app.post('/webhook', line.middleware(lineConfig), (req, res) => {
    Promise
      .all(req.body.events.map(handleEvent)) //mapは、rubyと一緒で配列に対して一個一個処理をする
      .then((result) => res.json(result));
});

const client = new line.Client(lineConfig);

function handleEvent(event) { //引数を取らなくてもreq.body.eventsの一個一個がeventの中に入っている
    if (event.type !== 'message' || event.message.type !== 'text') {
        return Promise.resolve(null);
    }
    const userMessage = event.message.text;
    InsertMessageIntoDb(userMessage)
    sentMessageToMultipleUsers(event)
    let replyMessage = '';
    if (userMessage == 'こんにちは') {
        replyMessage = '黙れ';
    }else{
        replyMessage = 'オラ、ワクワクするぞ！';
    };

    return client.replyMessage(event.replyToken, {
        type: 'text',
        text: replyMessage
    });
}
// グループの中でuserIdのuser以外のuser（複数）にmessageを送る
function sentMessageToMultipleUsers(event){
    const userId = event.source.userId; //messageを送ったuser情報を取得できる
    const query = `select users.id from users where not users.id = "${userId}"`; //ここでgroup_idとuser_idで絞り込む。wrapperでの書き方がわからなかった...
    connection.query(query, function (err, users) {
        const sendUserIds =  users.map(user => user.id);
        console.log(sendUserIds);
        console.log(`${sendUserIds}の複数ユーザーに送ります！！`);
        return client.multicast([userId], { // []の中にmessage内容を送るuser_idを入れる（複数） 便宜上、userIdにしている
            type: 'text',
            text: 'アイデアが発表されました！評価してくださいmm' //アイデアのリンクも送る
        });
    });
};

// 発話されたmessageをDBに保存する
function InsertMessageIntoDb(userMessage){
    db.ready(function () {
        const messagesTable = db.table("tweets");
        const newTweet = { quote: userMessage };
        messagesTable.save(newTweet).then(function(result){
            console.log(result); //resultに保存したrecordが格納されている
            console.log('保存しました。');
        });
    });
};

app.listen(PORT);
console.log(`Server running at ${PORT}`);
