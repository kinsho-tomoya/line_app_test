const mysql = require('mysql');
const config = {
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'triceps_development'
}

const connection = mysql.createConnection(config);

module.exports = connection;
